const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/*
	Check if the email already exists.
	Business Logic
	1. User find() method to find duplicate emails.
	2. Error-handling. If no duplicate found, return false, else return true.

	IMPORTANT NOTE: It is best practice to return a result is to use a boolean, or return an object/array of object, because string is limited in our backend, and can't be connected to our frontend.
*/
module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		}
		else{
			// No duplicate email found.
			return false;
		} // end else
	}); // end return
}; // end module.exports.checkEmailExists

/*
	Signing up
	Logic:
	1. Check if there is a duplicate email. 
		- If there is no duplicates: 
			- Create a new User object from the request body.
			- Encrypt the password.
			- Save the new user to database.
		- If there is a duplicate, return false.

*/
module.exports.signupUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result, err) => {
		if(err){
			return false;
		}
		else{
			if(result !== null && result.email === reqBody.email){
				// If there is duplicate
				return false;
			} // end if(result !== null && result.name === reqBody.email)
			else{
				let newUser = new User({
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				}); // end let newUser

				// Save the data
				return newUser.save().then((user, err) => {
					if(err){
						// Signup failed
						return false;
					}
					else{
						// Signup successful
						return true;
					}
				}); // end return newUser.save().then()
			} // end if(result !== null && result.name === reqBody.email)
		} // end else if(err)
	}); // end return User.findOne().then()
}; // end module.exports.signupUser

/*
	User authentication
	Business Logic:
	1. Check if the user's email exists in our database. 
		- If user does not exist, return false.
	2. If user exists, compare the password provided in the login form with the password stored in the database.
	3. Generate/return a jsonwebtoken if the user is successfully loggedin and return false if not.
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result === null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {accessToken: auth.createAccessToken(result.toObject())};
			}
			else{
				return false;
			}
		}
	}); // return User.findOne().then()
}; // end module.exports.loginUser

/*
	Retrieving a specific user
	Business Logic:
	1. Find the user with the ID specified in the URL (using findByID() function).
	2. Display the data EXCEPT THE PASSWORD.
*/
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id, {password: 0}).then((result, err) => {
		if(err || reqBody.id === ""){
			return false;
		}
		else{	
			if(result === null){
				return false;
			}
			else{
				return result;
			} // end else if(result === null)
		} // end else if (err || reqBody.id === "")
	}); // end User.findOne().then
}; // end module.exports.getProfile

/*
	Change password
	Logic
	- Find the user ID in the database using sessionUser.id.
	- Change the encrypted password set in the request body.
	- Save into the database.
*/
module.exports.changePassword = (sessionUser, reqBody) => {
	return User.findByIdAndUpdate(sessionUser.id, {password: bcrypt.hashSync(reqBody.password, 10)}).then((saved, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			return true;
		}
	}); // end return User.findByIdAndUpdate().then()
}; // end module.exports.changePassword

/*
	Set user as admin
	Logic:
	1. Check if the logged in user in the session(userData) is an admin.
		- If the user in the current session is an admin, get the ID from the URL, then set the user to admin and save it to database.
		- If the user is not an admin, deny request.
*/

module.exports.setAsAdmin = (data) => {
	return User.findById(data.sessionUserId).then((sessionUser, err) => {
		if(err){
			// Error handling
			return false;
		}
		else{
			// Determine if the session user is an admin
			if(sessionUser.isAdmin){
				return User.findByIdAndUpdate(data.changeToAdminId, {isAdmin: true}).then((result, err) => {
					if(err){
						return false;
					} // end if(err)
					else{
						return true;
					} // end else if(err)
				}); // end return User.findByIdAndUpdate().then()
			} // end if(sessionUser.isAdmin)
			else{
				return false;
			} // else if(sessionUser.isAdmin)
		}
	}); // return User.findById(userData.id)
}; // end module.exports.setAsAdmin

/*
	Add to cart
	Business Logic:
	- Check if there is a cart document that contains the session user's ID.
	- If not found, check if the item is active/in stock.
	- If active, create a new Cart object to save to the database.
	- If found, add the product ID from the products array, then calculate the price (if in stock)
*/
module.exports.addToCart = (sessionUser, cartItem) => {
	let totalAmount = 0;

	return User.findById(sessionUser.id).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			return Product.findById(cartItem.productId).then((product, err) => {
				if(err){
					return false;
				} // end if(err)
				else{
					// Check if the item is in stock
					if(product.isActive){
						user.cart.products.push({
							productId: product.id,
							name: product.name,
							quantity: cartItem.quantity,
							price: product.price
						});
						totalAmount += (product.price * cartItem.quantity);
						user.cart.totalAmount += totalAmount;
						// if(isNaN(user.cart.totalAmount)){
						// 	user.cart.totalAmount = 0;
						// 	user.cart.totalAmount = product.price;
						// }
						// else{
						// 	user.cart.totalAmount += product.price;
						// }
						return user.save().then((added, err) => {
							if(err){
								return false;
							} // end if(err)
							else{
								return true;
							} // end else if(err)
						}); // end return user.save().then()
					} // end if(product.isActive)
					else{
						return false;
					} // end else if(product.isActive)
				} // end else if(err)
			}); // end return Product.findById().then()
		} // end else if(err)
	}); // end return User.findById().then()
} // end module.exports.addToCart

module.exports.incrementCartItem = (sessionUser, productId) => {
	let incrementAmount = 0;
	return User.findOne({id: sessionUser.id, "cart.products.productId": productId}).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			user.cart.products = user.cart.products.map(product => {
				if(product.productId === productId){
					incrementAmount = product.price;
					product.quantity += 1;
				}
				return product;
			});
			user.cart.totalAmount += incrementAmount;
			return user.save().then((saved, err) => {
				if(err){
					return false;
				} // end if(err)
				else{
					return true;
				} // end else if(err)
			}); // end return user.save().then()
		} // end else if(err)
	}) // end return User.findById().then()
} // end module.exports.incrementCartItem

module.exports.decrementCartItem = (sessionUser, productId) => {
	let decrementAmount = 0;
	return User.findOne({id: sessionUser.id, "cart.products.productId": productId}).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			user.cart.products = user.cart.products.map(product => {
				if(product.productId === productId && product.quantity > 1){
					decrementAmount = product.price;
					product.quantity -= 1;
				}
				return product;
			});
			user.cart.totalAmount -= decrementAmount;
			return user.save().then((saved, err) => {
				if(err){
					return false;
				} // end if(err)
				else{
					return true;
				} // end else if(err)
			}); // end return user.save().then()
		} // end else if(err)
	}) // end return User.findById().then()
} // end module.exports.incrementCartItem



/*
	View cart items
	Business Logic
	1. Find the user data thru the session ID.
	2. Display all cart items.
*/
module.exports.viewCartItems = (sessionUser) => {
	return User.findById(sessionUser.id).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			return user.cart;
		} // end else if(err)
	}); // end return User.findById().then()
}; // end module.exports.viewCartItems

/*
	Remove cart items.
	Business logic:
	- Find the user in the users collection using session ID.
	- Find the item in the products collection using req.params.productId
	- Store the product price and product ID in a variable.
	- Filter the cart.products array using the product ID.
	- Deduct the item's price in the user.cart.totalAmount.
	- Save into the database.
*/
module.exports.removeItemInCart = (sessionUser, reqBody) => {
	// let productPrice = 0;
	// let isProductPriceAdded = await Product.findById(reqBody).then((product, err) => {
	// 	if(err){
	// 		return false;
	// 	}
	// 	else{
	// 		productPrice = product.price;
	// 		console.log(productPrice);
	// 		return true;
	// 	}
	// }); // end let productPrice
	// console.log(productPrice);
	// return "This item is under construction"
	return User.findById(sessionUser.id).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			let amountDeducted = 0;
			for(let i = 0; i <= user.cart.products.length - 1; i++){
				if(user.cart.products[i].productId === reqBody.productId){
					amountDeducted += user.cart.products[i].price * user.cart.products[i].quantity;
				} // if(user[i].cart.products.productId)
			} // end for(let i = 0; i < user.cart.products.length)
			user.cart.totalAmount -= amountDeducted;
			user.cart.products = user.cart.products.filter(val => val.productId !== reqBody.productId);

			return user.save().then((saved, err) => {
				if(err){
					return false;
				}
				else{
					return true;
				}
			});
		} // end else if(err)
	}); // end return User.findById().then()
}; // end module.exports.removeItemInCart
